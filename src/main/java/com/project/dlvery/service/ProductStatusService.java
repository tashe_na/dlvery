package com.project.dlvery.service;

import java.util.List;

import com.project.dlvery.entity.ProductStatus;

public interface ProductStatusService {
	List<ProductStatus> listAllProductStatus();
	
}
